Scoville
--------

Project Page:
http://drupal.org/project/scoville

By Garrett Albright
http://drupal.org/user/191212

Sponsored by PINGV, Inc. Awesome!
http://pingv.com/

Installation & Configuration
----------------------------

For full installation and configuration instructions, please see this page in
the Drupal online manual:
http://drupal.org/node/1804274
