<?php

/**
 * Implements hook_schema().
 */
function scoville_schema() {
  return array(
    'scoville' => array(
      'description' => 'Holds data relating to the hotness of nodes.',
      'fields' => array(
        'nid' => array(
          'description' => 'The {node}.nid of the node to store hotness for.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
        'last_total' => array(
          'description' => 'The {node_counter}.totalcount value for the last time hotness for this node was calculated.',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
          'size' => 'big',
        ),
        'hotness' => array(
          'description' => 'Current hotness level for this node.',
          'type' => 'float',
          'size' => 'big',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'primary key' => array('nid'),
      'foreign keys' => array(
        'nid' => array(
          'table' => 'node',
          'columns' => array('nid' => 'nid'),
        ),
      ),
    ),
  );
}

/**
 * Implements hook_uninstall().
 */
function scoville_uninstall() {
  variable_del('scoville_last_run');
  variable_del('scoville_cool_rate');
  variable_del('scoville_hit_points');
  variable_del('scoville_cold_point');
  variable_del('scoville_types');
}
